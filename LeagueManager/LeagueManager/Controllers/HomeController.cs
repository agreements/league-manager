﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeagueManager.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Stuff About our management stuff.....";

            return View();
        }

        public ActionResult BaseBall()
        {
            ViewBag.Message = "Baseball landing page.";

            return View();
        }
        public ActionResult BasketBall()
        {
            ViewBag.Message = "Basketball landing page.";

            return View();
        }
        public ActionResult CheerLeading()
        {
            ViewBag.Message = "Cheerleading landing page.";

            return View();
        }
        public ActionResult FootBall()
        {
            ViewBag.Message = "Football landing page.";

            return View();
        }
            public ActionResult Soccer()
        {
            ViewBag.Message = "Soccer landing page.";

            return View();
        }
        public ActionResult SoftBall()
        {
            ViewBag.Message = "Softball landing page.";

            return View();
        }
        public ActionResult Coaches()
        {
            
                ViewBag.Message = "Coaches Landing Page";
            
            
            return View();
        }
    }
}